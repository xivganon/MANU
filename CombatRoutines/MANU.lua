--- Crafting ACR
--@module MANU
local MANU = {}

if not _G["aa"] then
	return nil
end


-- -------------------------------------------------------------------------- --
-- ANCHOR                        Internal Info                                --
-- -------------------------------------------------------------------------- --

MANU._VERSION = "0.1.0"
MANU._NAME = "Manufacturer"
MANU._DESCRIPTION = "Crafting ACR"
MANU._URL = ""
MANU._ID = ""

d("[AnonAddons] Manufacturer Loaded")
-- io.popen('cmd /c copy /y '..GetLuaModsPath()..[[AAManufacturer\MANU.lua]]..' '..GetLuaModsPath()..[[ACR\CombatRoutines\Manufacturer.lua]])

-- -------------------------------------------------------------------------- --
-- ANCHOR                       Module Variables                              --
-- -------------------------------------------------------------------------- --

MANU.GUI = {
	windowOpen = true,
	windowVisible = true,
	overlayVisible = true,
	mainTabs = {
		tabs = {
			"Manage Recipes",
			"Manage Macros"
		},
		tabSelected = 2,
	},
	subTabs = {
		tabs = { },
		tabSelected = 2,
	},
	newMacroText = "",
	newMacroName = "",
	macroName = "",
	macroView = { },
	newItemId = 0,
	newItemQuality = 0,
	newItemDurability = 0,
}

MANU.classes = {
	[FFXIV.JOBS.CARPENTER] = true,
	[FFXIV.JOBS.BLACKSMITH] = true,
	[FFXIV.JOBS.ARMORER] = true,
	[FFXIV.JOBS.GOLDSMITH] = true,
	[FFXIV.JOBS.LEATHERWORKER] = true,
	[FFXIV.JOBS.WEAVER] = true,
	[FFXIV.JOBS.ALCHEMIST] = true,
	[FFXIV.JOBS.CULINARIAN] = true,
}

MANU.settings = {
	macros = { },
	items = { }
}

MANU.state = {
	stepOffset = 0,
	rotation = {},
	macroText = 'sup'
}

MANU.actions = { }

-- -------------------------------------------------------------------------- --
--                               Initialization                               --
-- -------------------------------------------------------------------------- --

--- Ran once when the ACR loads the profile
function MANU.OnLoad()

	local acList = ActionList:Get(1)
    for actionId, action in pairs(acList) do
        if action.job == Player.job and action.usable == true then
            MANU.actions[action.name] = action
        end
	end

	acList = ActionList:Get(9)
    for actionId, action in pairs(acList) do
        if action.job == Player.job and action.usable == true then
            MANU.actions[action.name] = action
        end
	end

	for k,v in pairs(MANU.actions) do d(k) end

	MANU.settings.macros = Settings.MANU.macros or {}
	MANU.settings.items = Settings.MANU.items or {}

	aa["MANU"] = {
		GetInfo = function()
			return {
				Version = MANU._VERSION,
				Name = MANU._NAME,
				Description = MANU._DESCRIPTION,
				Url = MANU._URL,
				Id = MANU._ID,
			}
		end
	}

	QueueEvent("AnonAddons.InitDone", "MANU", nil)
	d("[AnonAddons] Manufacturer ACR Ready")
end

-- -------------------------------------------------------------------------- --
--                                  Rendering                                 --
-- -------------------------------------------------------------------------- --

function MANU.OnOpen()
	MANU.GUI.windowOpen = true
end

function MANU.Draw()
	if (MANU.GUI.windowOpen) then
		local changed = false
		GUI:SetNextWindowPosCenter(GUI.SetCond_FirstUseEver)
		GUI:SetNextWindowSize(600,480)
		MANU.GUI.windowVisible, MANU.GUI.windowOpen = GUI:Begin(
			"Manufacturer##MANUAcrOptions",
			MANU.GUI.windowOpen,
			GUI.WindowFlags_NoScrollbar + GUI.WindowFlags_NoResize
		)
		if (MANU.GUI.windowVisible) then
			local fullWidth, fullHeight = GUI:GetContentRegionAvail()

			aa.DrawTabs('##MANUMain',MANU.GUI.mainTabs, fullWidth, 30)

----------------------------- Manage Recipes ----------------------------- --

			if MANU.GUI.mainTabs.tabSelected == 1 then
				GUI:BeginChild('##MANUMacroList', fullWidth/2 -5, 0)
					for name, macro in pairs(MANU.settings.macros) do
						if GUI:Button(aa.WrapText(name,45)) then
							MANU.GUI.macroName = name
							MANU.GUI.macroView = macro
						end
					end
				GUI:EndChild()
				GUI:SameLine()
				GUI:BeginChild('##MANURecipeAssociation', fullWidth/2 -5, 0)
					if not string.empty(MANU.GUI.macroName) then
						aa.PushStyle(aa.Layouts.transparentButton)
						GUI:Button(MANU.GUI.macroName)
						aa.PopStyle(aa.Layouts.transparentButton)

						GUI:Separator()

						MANU.GUI.newItemId = GUI:InputInt('ID##MANUInputId',
							MANU.GUI.newItemId)
						if MANU.GUI.newItemId < 0 then MANU.GUI.newItemId = 0 end

						if ( MANU.GUI.newItemId ~= 0 or
							(MANU.GUI.newItemDurability ~= 0
							and MANU.GUI.newItemQuality ~= 0)
						) then
							if GUI:Button('Link Recipe##MANURecipeLink',GUI:GetContentRegionAvailWidth(),15) then
								if MANU.GUI.newItemId ~= 0 then
									MANU.settings.items[MANU.GUI.newItemId] = MANU.GUI.macroName
									if not table.valid(MANU.settings.macros[MANU.GUI.macroName]['items']) then
										MANU.settings.macros[MANU.GUI.macroName]['items'] = {}
									end
									table.insert(MANU.settings.macros[MANU.GUI.macroName]['items'], 1, MANU.GUI.newItemId)
									aa.SaveSettings('MANU', MANU.settings)
									MANU.GUI.newItemDurability = 0
									MANU.GUI.newItemId = 0
									MANU.GUI.newItemQuality = 0
								end
							end
						end

						GUI:Separator()

						aa.PushStyle(aa.Layouts.transparentButton)
						GUI:Button('Recipes Linked',GUI:GetContentRegionAvailWidth(),15)
						aa.PopStyle(aa.Layouts.transparentButton)
						GUI:BeginChild('##MANULinkedRecipes',0,0, true)
							if table.valid(MANU.settings.macros[MANU.GUI.macroName].items) then
								for index, item in pairs(MANU.settings.macros[MANU.GUI.macroName].items) do
									if (_G['AceLib']) then
										local datItem = Inventory:GetItemDetails(item)
										if datItem then
											GUI:Button(item..': '..datItem.name)
										else
											GUI:Button('ID: '..item)
										end
									else
										GUI:Button('ID: '..item)
									end
									GUI:SameLine()
									if GUI:Button([[X##]]..item) then
										table.remove(MANU.settings.macros[MANU.GUI.macroName].items, index)
									end
								end
							end
						GUI:EndChild()
					end
				GUI:EndChild()
			end

------------------------------ Manage Macro ------------------------------ --

			if MANU.GUI.mainTabs.tabSelected == 2 then

				MANU.GUI.subTabs.tabs = {"Macro List", "New Macro"}
				aa.DrawTabs('##MANUSub',MANU.GUI.subTabs, fullWidth, 18)

				if MANU.GUI.subTabs.tabSelected == 1 then
					GUI:BeginChild('##MANUMacroList', (fullWidth/3)*2 -5, 0)
						for name, macro in pairs(MANU.settings.macros) do
							if GUI:Button(aa.WrapText(name,45)) then
								MANU.GUI.macroView = macro.actions
							end
							GUI:SameLine()
							if GUI:ImageButton("##MANUDeleteMacro"..name, aa.Icons.trash,13,13	) then
								MANU.settings.macros[name] = nil
								MANU.GUI.macroName = ""
								MANU.GUI.macroView = {}
								aa.SaveSettings('MANU', MANU.settings)
							end
						end
					GUI:EndChild()
					GUI:SameLine()
					GUI:BeginChild('##MANUMacroView', fullWidth/3 -5, 0)
						for index, actionName in ipairs(MANU.GUI.macroView) do
							GUI:Text(string.format("%2s",index))
							GUI:SameLine()
							GUI:Button(actionName..'##'..index, 150, 25)
						end
					GUI:EndChild()
				end

				if MANU.GUI.subTabs.tabSelected == 2 then
					-- Text Input
					MANU.GUI.newMacroText, changed =
					GUI:InputTextMultiline(
						"##MacroInput", MANU.GUI.newMacroText,
						(fullWidth/3)*2 - 5, fullHeight-90,
						GUI.InputTextFlags_NoHorizontalScroll
					)

					GUI:SameLine()

					GUI:BeginChild('##MANUMacroSteps', fullWidth/3-5, fullHeight-90)
					-- Macro buttons
					if not string.empty(MANU.GUI.newMacroText) then
						for index, actionName in ipairs(MANU.ParseMacroText(MANU.GUI.newMacroText)) do
							GUI:Text(string.format("%2s",index))
							GUI:SameLine()
							GUI:Button(actionName..'##'..index, 150, 25)
						end
					end

					GUI:EndChild()
					if GUI:Button('Save Macro As:') then
						if not string.empty(MANU.GUI.newMacroName) then
							if not table.valid(MANU.settings.macros[MANU.GUI.newMacroName]) then
								MANU.settings.macros[MANU.GUI.newMacroName] = {items = {}, actions = {}}
							end
							MANU.settings.macros[MANU.GUI.newMacroName].actions = MANU.ParseMacroText(MANU.GUI.newMacroText)
							aa.SaveSettings('MANU', MANU.settings)
						end
					end
					GUI:SameLine()
					MANU.GUI.newMacroName = GUI:InputText('##MANUNewMacroName',
						MANU.GUI.newMacroName,
						GUI.InputTextFlags_AlwaysInsertMode)
				end

			end

		end
		GUI:End()
	end
	if (MANU.GUI.toggleVisible) then

	end
end

-- -------------------------------------------------------------------------- --
-- ANCHOR                             Logic                                   --
-- -------------------------------------------------------------------------- --

function MANU.OnUpdate(event, tickcount)
	if not IsControlOpen("Synthesis") then
		MANU.state.stepOffset = 0
	end
end

function MANU.Cast()
	if IsControlOpen("Synthesis") then
		synth = GetControl("Synthesis"):GetData()
		if MANU.settings.items[synth.itemid] then
			local macro = MANU.settings.items[synth.itemid]
			if MANU.settings.macros[macro] then
				macro = MANU.settings.macros[macro]
				d('[MANU] Want to Cast '..MANU.actions[macro.actions[synth.step + MANU.state.stepOffset]].name..' on step '..synth.step+MANU.state.stepOffset)
				if MANU.actions[macro.actions[synth.step + MANU.state.stepOffset]]:IsReady(Player) then
					d('[MANU] Casting '..MANU.actions[macro.actions[synth.step + MANU.state.stepOffset]].name)
					MANU.actions[macro.actions[synth.step + MANU.state.stepOffset]]:Cast(Player)
					if MANU.actions[macro.actions[synth.step + MANU.state.stepOffset]].name == "Heart and Soul" then
						MANU.state.stepOffset = MANU.state.stepOffset + 1
					end
					d('[MANU] Step Offset: '..MANU.state.stepOffset)
					return true
				end
			end
		end
	end
	return false
end

function MANU.ParseMacroText(text)
	local stringTable = string.totable(text, '\n')
	local returnTable = {}
	if table.valid(stringTable) then
		for index, string in ipairs(stringTable) do
			if string.starts(string, "/ac") then
				if string.match(string, "\".+\"") then
					local i,j = string.find(string, "\".+\"")
					table.merge(returnTable, {string.sub(string, i+1, j-1)}, true)
				elseif string.match(string, " %a+ ") then
					local i,j = string.find(string, " %a+ ")
					table.merge(returnTable, {string.sub(string, i+1, j-1)}, true)
				end
			end
		end
	else
		return {}
	end
	return returnTable
end

-- -------------------------------------------------------------------------- --
--                           Registration and Return                          --
-- -------------------------------------------------------------------------- --

return MANU
